package ru.sberbank.jd.lesson12.entitys;

import org.junit.Test;
import ru.sberbank.jd.lesson13_14.entitys.Category;

import java.sql.SQLException;
import java.util.List;

public class CategoryTest {
    //создание таблицы
    @Test
    public void createTable() throws SQLException {
        Category.createTable();
    }

    //вставка тестовых данных
    @Test
    public void insertTable() throws SQLException {
        Category.insertDataTable();
    }

    @Test
    public void deleteData() throws SQLException {
        Category.deleteData();
    }

    //вывод всех данных из таблицы
    @Test
    public void selectData() throws SQLException {
        List<Category> result = Category.selectData();
        System.out.println(result);
    }

    //вставка нового значения
    @Test
    public void insert() throws SQLException {
        Category.insert(Category.builder()
                .id(123)
                .name("Фрукты")
                .build());
    }

    //удаление таблицы
    @Test
    public void dropTable() throws SQLException {
        Category.dropTable();
    }
}