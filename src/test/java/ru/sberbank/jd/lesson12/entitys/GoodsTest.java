package ru.sberbank.jd.lesson12.entitys;

import org.junit.Test;
import ru.sberbank.jd.lesson13_14.entitys.Category;
import ru.sberbank.jd.lesson13_14.entitys.Goods;

import java.sql.SQLException;
import java.util.List;

import static ru.sberbank.jd.lesson13_14.entitys.Goods.selectGoodsAndCategory;

public class GoodsTest {
    //создание таблицы
    @Test
    public void createTable() throws SQLException {
        Goods.createTable();
    }

    //вывод все значений из таблицы
    @Test
    public void selectdata() throws SQLException {
        List<Goods> goods = Goods.selectData();
        System.out.println(goods);
    }

    //удаление всех данных
    @Test
    public void deleteData() throws SQLException {
        Goods.deleteData();
        selectfromTable();
    }

    //удаление таблицы
    @Test
    public void dropTable() throws SQLException {
        Goods.dropTable();
    }

    //вставка нового значения
    @Test
    public void insert() throws SQLException {
        Goods.insert(Goods.builder()
                .id(123)
                .categoryId(12)
                .name("Перфоратор")
                .cost(2303.45f)
                .build());
    }

    //проверка соединения и извлечения данных из нескольких таблиц
    @Test
    public void selectfromTable() throws SQLException {
        System.out.println(Goods.selectData());
        List<Goods> goods = Goods.select(1);
        System.out.println(goods);
    }

    @Test
    public void selectfromTables() throws SQLException {
        selectGoodsAndCategory(3);
    }

    @Test
    public void testAll() throws SQLException {
        Goods.dropTable();
        Goods.createTable();
        System.out.println(Goods.selectData());
        Goods.insertDataTable();
        System.out.println(Goods.selectData());
        insert();
        System.out.println(Goods.selectData());
        Goods.selectGoodsAndCategory(2);
    }
}