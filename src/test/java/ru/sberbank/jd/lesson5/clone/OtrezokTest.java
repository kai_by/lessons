package ru.sberbank.jd.lesson5.clone;

import org.junit.Test;

import static org.junit.Assert.*;

public class OtrezokTest {

    @Test
    public void testClone() {
        Otrezok original=new Otrezok(1,1,2,2);
        Otrezok clone=original.clone();
        System.out.println(original.hashCode()+" "+clone.hashCode());
    }
}