package ru.sberbank.jd.lesson5.immutable_interface;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AnimalTest {

    @Test
    public void getName() {
        NoChange noChange= new Animal("Fabricio",5,Sex.MALE);
        String newName="Tor";
        //noChange.setName(newName); ошибка компиляции, хотя в классе метод определен
        System.out.println(noChange.getName());
    }

    @Test
    public void getAge() {
        Animal animal= new Animal("Fabricio",5,Sex.MALE);
        int newAge=15;
        //noChange.setAge(newAge); ошибка компиляции, хотя в классе метод определен
        System.out.println(animal.getAge());
    }

    @org.junit.Test
    public void getSex() {
        NoChange noChange= new Animal("Fabricio",5,Sex.MALE);
        Sex newSex=Sex.FEMALE;
        assertEquals("мужской", noChange.getSex().getDescription());
    }

    @Test
    public void setSex() {
        NoChange noChange= (NoChange) new Animal("Fabricio",5,Sex.MALE);
        Sex newSex=Sex.FEMALE;
        //noChange.setSex(newSex); ошибка компиляции, хотя в классе метод определен
        Animal animal=new Animal("Fabricio",5,Sex.MALE);
        animal.setSex(newSex); //а здесь мы можем поменять пол животного
        Assert.assertEquals(Sex.FEMALE.getDescription(),animal.getSex().getDescription());
    }
}