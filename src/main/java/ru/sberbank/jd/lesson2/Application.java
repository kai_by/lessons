package ru.sberbank.jd.lesson2;

public class Application {
    public static void main(String[] args) {
        try(Lesson2 lesson2=new Lesson2()) {
            lesson2.Hi();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println("Что-то при встрече пошло не так...");
        }
        finally {
            System.out.println("Ладно. Все нормально. Идем дальше.");
        }
    }
}
