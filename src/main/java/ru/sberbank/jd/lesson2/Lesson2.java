package ru.sberbank.jd.lesson2;

public class Lesson2 implements AutoCloseable{

    public Lesson2()
    {
        System.out.println("Я дружелюбный класс.");
    }

    public void Hi()
    {
        System.out.println("Пытаюсь поздороваться.\nПривет, незнакомый класс!");
        //int a=24,b=0,c;
        //c=a/b;
        throw new RuntimeException("Другой класс отвернулся...");
    }
    @Override
    public void close() throws Exception {
        System.out.println("К сожалению, не поздоровался, а почему-то отвернулся!");
    }
}
