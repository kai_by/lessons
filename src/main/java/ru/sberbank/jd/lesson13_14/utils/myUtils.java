package ru.sberbank.jd.lesson13_14.utils;

import java.sql.*;

public class myUtils {
    public static Connection GetConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:~/sql;MODE=PostgreSQL","sa","");
    }
    public static boolean Execute(String SQL) throws SQLException {
        Connection connection = myUtils.GetConnection();
        Statement statement=connection.createStatement();
        return statement.execute(SQL);
    }
    public static ResultSet ExecuteSQL(String SQL) throws SQLException {
        Connection connection = myUtils.GetConnection();
        Statement statement=connection.createStatement();
        return getStatement().executeQuery(SQL);
    }

    public static Statement getStatement() throws SQLException {
        Connection connection = myUtils.GetConnection();
        return connection.createStatement();
    }
}
