package ru.sberbank.jd.lesson13_14.entitys;

import lombok.Builder;
import lombok.Value;
import ru.sberbank.jd.lesson13_14.utils.myUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Builder
@Value
public class Category {
    String name;
    long id;

    public static void createTable() throws SQLException {
        String command = "CREATE TABLE IF not exists Category (id int, name varchar(250));\n";
        myUtils.Execute(command);
    }

    public static void dropTable() throws SQLException {
        String command = "DROP TABLE IF exists Category;\n";
        myUtils.Execute(command);
    }

    public static void insertDataTable() throws SQLException {
        String command = "INSERT into category values (41, 'Овощи');\n" +
                "INSERT into category values (12, 'Инструменты');\n" +
                "INSERT into category values (33, 'Игрушки');\n" +
                "INSERT into category values (24, 'Обувь');";
        myUtils.Execute(command);
    }

    public static void deleteData() throws SQLException {
        String command = "delete from Category;";
        myUtils.ExecuteSQL(command);
    }

    public static List<Category> selectData() throws SQLException {
        List<Category> result = new ArrayList<>();
        String command = "select * from category;";
        ResultSet rs = myUtils.ExecuteSQL(command);
        while (rs.next()) {
            result.add(Category.builder()
                    .id(rs.getInt(1))
                    .name(rs.getString(2))
                    .build());
        }
        return result;
    }
    public static void insert(Category category) throws SQLException {
        String sql="INSERT INTO Category(id, name) VALUES ( ?, ?)";
        PreparedStatement stat=myUtils.GetConnection().prepareStatement(sql);
        stat.setLong(1, category.getId());
        stat.setString(2,category.getName());
        stat.executeUpdate();
    }

}
