package ru.sberbank.jd.lesson13_14.entitys;

import lombok.Builder;
import lombok.Value;
import ru.sberbank.jd.lesson13_14.utils.myUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Value
@Builder
public class Goods {
    String name;
    float cost;
    int id;
    long categoryId;

    public static void createTable() throws SQLException {
        String command = "CREATE TABLE IF not exists GOODS (id int, name varchar(250), categoryId number,cost number);\n";
        myUtils.Execute(command);
    }

    public static void insertDataTable() throws SQLException {
        String command = "INSERT into GOODS values (1, 'Капуста', 41, 20);\n" +
                "INSERT into GOODS values (2, 'Плоскогубцы', 12, 1300);\n" +
                "INSERT into GOODS values (3, 'Радиоуправляемая машинка', 33, 2200);\n" +
                "INSERT into GOODS values (4, 'Тапочки', 24, 200);";
        myUtils.Execute(command);
    }

    public static void insert(Goods good) throws SQLException {
        String sql = "insert into goods(id,name,categoryId,cost) values(?,?,?,?)".toUpperCase();
        PreparedStatement stat = myUtils.GetConnection().prepareStatement(sql);
        stat.setLong(1, good.getId());
        stat.setString(2, good.getName());
        stat.setLong(3, good.getCategoryId());
        stat.setFloat(4, good.getCost());
        stat.execute();
    }

    public static void deleteData() throws SQLException {
        String command = "delete from GOODS;";
        myUtils.getStatement().execute(command);
    }

    public static List<Goods> selectData() throws SQLException {
        List<Goods> result = new ArrayList<>();
        String command = "select * from goods;";
        ResultSet rs = myUtils.ExecuteSQL(command);
        while (rs.next()) {
            result.add(Goods.builder()
                    .id(rs.getInt(1))
                    .name(rs.getString(2))
                    .categoryId(rs.getInt(3))
                    .cost(rs.getFloat(4)).build());
        }
        return result;
    }

    public static List<Goods> select(int id) throws SQLException {
        String sql = "select * from goods where id = ?";
        List<Goods> result = new ArrayList<>();
        PreparedStatement stat = myUtils.GetConnection().prepareStatement(sql);
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        while (rs.next()) {
            result.add(Goods.builder()
                    .id(rs.getInt("ID"))
                    .cost(rs.getFloat("COST"))
                    .name(rs.getString("NAME"))
                    .categoryId(rs.getInt("CATEGORYID"))
                    .build());
        }
        return result;
    }

    public static void selectGoodsAndCategory(int id) throws SQLException {
        String sql = "select g.id, g.name, g.cost, c.name categoryname from goods g, category c where g.categoryid=c.id and g.id = ?";
        PreparedStatement stat = myUtils.GetConnection().prepareStatement(sql);
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        ResultSetMetaData metaData = rs.getMetaData();
        LinkedHashMap<String, String> keyvalues = new LinkedHashMap<>();
        while (rs.next())
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
//                System.out.println(String.format("Наименование поля:%s; его значение:%s.", metaData.getColumnName(i), rs.getString(i)));
                keyvalues.put(metaData.getColumnName(i), rs.getString(i));
            }
        System.out.println(keyvalues);
    }

    public static void dropTable() throws SQLException {
        myUtils.getStatement().execute("DROP TABLE IF exists goods");
    }
}
