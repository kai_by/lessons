package ru.sberbank.jd.lesson4;

public class Minus extends Operation implements Calc{

    public Minus(int value) {
        super(value);
        this.typeOperatinon=TypeOperatinon.MINUS;
    }

    @Override
    public int Calculation(int value) {
        return this.value-value;
    }
}
