package ru.sberbank.jd.lesson4;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum TypeOperatinon {
    PLUS(1,"Summa"),
    MINUS(2,"Decrement"),
    DIVIZED(3,"Divized"),
    Not_Exists(10,"Неизвестная метрика");

    private final int number;
    private final String name;
}
