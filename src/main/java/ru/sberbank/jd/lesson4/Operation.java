package ru.sberbank.jd.lesson4;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class Operation {
    protected String name;
    protected TypeOperatinon typeOperatinon;
    protected int value;

    public Operation(int value)
    {
        this.value=value;
    }
}
