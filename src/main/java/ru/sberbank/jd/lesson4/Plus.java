package ru.sberbank.jd.lesson4;

public class Plus extends Operation implements Calc{

    public Plus(int value) {
        super(value);
        this.typeOperatinon=TypeOperatinon.PLUS;
    }

    @Override
    public int Calculation(int value) {
        return this.value+value;
    }
}
