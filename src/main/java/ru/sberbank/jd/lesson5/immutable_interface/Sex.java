package ru.sberbank.jd.lesson5.immutable_interface;

import lombok.Getter;

@Getter
public enum Sex {
    MALE("мужской"),
    FEMALE("женский");

    private String description;

    private Sex(String description) {
        this.description=description;
    }
}