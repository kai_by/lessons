package ru.sberbank.jd.lesson5.immutable_interface;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class Animal implements NoChange{
    private String name;
    private int age;
    private Sex sex;

    public Animal(String name, int age, Sex sex)
    {
        this.name=name;this.age=age;this.sex=sex;
    }
}
