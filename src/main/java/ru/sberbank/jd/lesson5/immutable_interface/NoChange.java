package ru.sberbank.jd.lesson5.immutable_interface;

public interface NoChange {
    public Sex getSex();
    public int getAge();
    public String getName();
}
