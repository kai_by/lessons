package ru.sberbank.jd.lesson5.clone;

public class Otrezok extends Point {
    public int x2;
    public int y2;

    public Otrezok(int x,int y,int x2,int y2){
        this.x=x;
        this.y=y;
        this.x2=x2;
        this.y2=y2;
    }
    public Otrezok(Otrezok otrezok) {
        super(otrezok);
        if (otrezok!=null)
        {
            this.x2=otrezok.x2;
            this.y2=otrezok.y2;
            this.y=otrezok.y;
            this.x=otrezok.x;
        }
    }
    @Override
    public Otrezok clone() {
        return new Otrezok(this);
    }
}
