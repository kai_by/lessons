package ru.sberbank.jd.lesson5.clone;

public abstract class Point {
    public int x;
    public int y;

    public Point(){}

    public Point(Point target)
    {
        if (target!=null){
            this.x=target.x;
            this.y=target.y;
        }
    }

    public abstract Point clone();
}
